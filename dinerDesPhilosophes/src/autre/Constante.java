package autre;

import java.awt.Color;

public class Constante {
    public static final String QUI_EST_LE_MEILLEUR = "Nettoyage du code fait par David Gilbert";
    
    public static final int AUCUN_PHILOSOPHES = 0;
    public static final int NOMBRE_DE_PHILOSOPHES = 5;
    public static final int NOMBRE_DE_PHILOSOPHE_MOINS_LUI_MEME = 1;
    public static final int TAILLE_INTERFACE_TABLE_PHILOSOPHE = 200;
    
    /** Modifier pour mieux voir */
    public static final int TEMPS_MAXIMAL_PENSER = 4000;
    public static final int TEMPS_MAXIMAL_MANGER = 3000;
    
    public static final int ID_PAR_DEFAUT_PHILOSOPHE = -1;
    public static final int ID_PHILOSOPHE_1 = 0;
    public static final int ID_PHILOSOPHE_2 = 1;
    public static final int ID_PHILOSOPHE_3 = 2;
    public static final int ID_PHILOSOPHE_4 = 3;
    public static final int ID_PHILOSOPHE_5 = 4;
    
    public static final long TEMPS_DE_REPOS = 500l;
    
    public static final double DEMI_CERCLE = 180.0;
    
    public static final boolean CHOPSTICK_LIBRE = true;
    public static final boolean CHOPSTICK_PRIT = false;
    
    public static final Color COULEUR_INTERFACE = Color.darkGray;
    public static final Color COULEUR_AUCUN_PHILOSOPHE = Color.black;
    public static final Color COULEUR_PHILOSOPHE_1 = Color.red;
    public static final Color COULEUR_PHILOSOPHE_2 = Color.blue;
    public static final Color COULEUR_PHILOSOPHE_3 = Color.green;
    public static final Color COULEUR_PHILOSOPHE_4 = Color.yellow;
    public static final Color COULEUR_PHILOSOPHE_5 = Color.white;

    public static final String TEXTE_NOM_PHILOSOPHES = "Philosophes #";
    public static final String TEXTE_TITRE_PROJET = "Diner des philosophes";
    
    public static final boolean ECRAN_AJUSTABLE = false;
    public static final boolean LES_PHILOSOPHES_DINE = true;

    /**
     * Comprends pas ce qu'ils font mais je les ai mis dans des constantes pour pas avoir
     * de magic Numbers
     */
    public static final int UN_VINGTQUATRIEME_CERCLE = 15;
    public static final int UN_CINQUIEME_CERCLE = 72;
    public static final int UN_DIXIEME_CERCLE = 36;
    
    public static final int COORD_CINQ = 5;
    public static final int COORD_DIX = 10;
    public static final int COORD_QUARANTE = 40;
    public static final int COORD_SOIXANTEDIX = 70;

    public static final int POINT_VINGT = 20;
}
