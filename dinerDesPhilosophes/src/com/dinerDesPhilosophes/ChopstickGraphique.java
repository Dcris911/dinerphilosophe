package com.dinerDesPhilosophes;

import static autre.Constante.*;

import java.awt.*;

public class ChopstickGraphique extends DinerGraphique{
  private Point centre;
  private Point coordDebut;
  private Point coordFin;

  
  ChopstickGraphique (int id, Point centre, Point coordDebut, Point coordFin) {    
      super();
      initialisationCentre(centre);
      initialisationAngle(id);
      
      configurationPointCoordonne(coordDebut, coordFin, centre);
  }
  
    private void initialisationCentre (Point centre) {
          this.centre = centre;
    }
    
    private void initialisationAngle (int id) {
        this.angle = UN_CINQUIEME_CERCLE * id - UN_DIXIEME_CERCLE;
    }
    
    private void configurationPointCoordonne (Point coordDebut, Point coordFin, Point centre) {
        configurationCoordonneDebut(coordDebut);
        configurationCoordonneFin(coordFin);
    }
  
    private void configurationCoordonneDebut (Point coordDebut) {
        this.coordDebut = new Point(rotation(coordDebut.x, coordDebut.y, 
                this.centre.x, this.centre.y, angle));
        this.coordDebut.y += UN_VINGTQUATRIEME_CERCLE;
    }
      
    private void configurationCoordonneFin (Point coordFin) {
        this.coordFin = new Point(rotation(coordFin.x, coordFin.y, 
        this.centre.x, this.centre.y, angle));
        this.coordFin.y += UN_VINGTQUATRIEME_CERCLE;
    }
  
    public void dessine (Graphics g) {
      g.setColor(couleur);
      g.drawLine(coordDebut.x, coordDebut.y, coordFin.x, coordFin.y);
    }

  //TODO No comprendo mais fonctionno -> pas comprendre = incapable refactor
    private Point rotation (int x, int y, int bX, int bY, int angle) {
        Point point = new Point();
        double a = Math.PI/(DEMI_CERCLE/angle);
        point.x = (int) (x * Math.cos(a) - y * Math.sin(a) 
            - Math.cos(a) * bX + Math.sin(a) * bY + bX);
        point.y = (int) (x * Math.sin(a) + y * Math.cos(a) 
            - Math.sin(a) * bX - Math.cos(a) * bY + bY);
        return(point);
    }
}
  
