package com.dinerDesPhilosophes;

import static autre.Constante.*;

import java.awt.Color;

public class DinerGraphique {
    
    protected int idPhilosophe;
    protected Color couleur;
    protected int angle;
    
    public DinerGraphique () {
        this.idPhilosophe = ID_PAR_DEFAUT_PHILOSOPHE;
        definiCouleurPhilosophe(idPhilosophe);
    }
    
    public Color definiCouleurPhilosophe (int idPhilosophe) {
        this.idPhilosophe = idPhilosophe;
        
        if(this.idPhilosophe == ID_PAR_DEFAUT_PHILOSOPHE) {
          this.couleur = COULEUR_AUCUN_PHILOSOPHE;
        } else if(this.idPhilosophe == ID_PHILOSOPHE_1) {      
          this.couleur = COULEUR_PHILOSOPHE_1;
        } else if(this.idPhilosophe == ID_PHILOSOPHE_2) {      
          this.couleur = COULEUR_PHILOSOPHE_2;
        } else if(this.idPhilosophe == ID_PHILOSOPHE_3) {      
          this.couleur = COULEUR_PHILOSOPHE_3;
        } else if(this.idPhilosophe == ID_PHILOSOPHE_4) {      
          this.couleur = COULEUR_PHILOSOPHE_4;
        } else if(this.idPhilosophe == ID_PHILOSOPHE_5) {      
          this.couleur = COULEUR_PHILOSOPHE_5;
        }
        
        return this.couleur;
    }
}
