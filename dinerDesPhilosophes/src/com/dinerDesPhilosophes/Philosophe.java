package com.dinerDesPhilosophes;

import static autre.Constante.*;

public class Philosophe extends Thread {
    private TableGraphique tableManger;
    private int gauche;
    private int droite;
    private int idDuPhilosophe;


    public Philosophe (int id, TableGraphique table, int gauche, int droite) {
        this.idDuPhilosophe = id;
        this.tableManger = table;
        this.gauche = gauche;
        this.droite = droite;
        setName(TEXTE_NOM_PHILOSOPHES + id);
    }

    public void run () {
        repos(TEMPS_DE_REPOS);
        while (LES_PHILOSOPHES_DINE) {
            philosophePense();
            philosopheDevientAffamer();
            philosopheMange();
        }
    }
    

    private void philosophePense (){
        tableManger.enTrainDeReflechir(idDuPhilosophe);
        repos((long)(Math.random()*TEMPS_MAXIMAL_PENSER));
    }
    
    private void philosopheDevientAffamer (){
        tableManger.devientAffame(idDuPhilosophe);
    }
    
    private void philosopheMange () {
        philosophePrendChopstick();
        philosopheMangeAvecChopstick();
        philosophelibereChopstick();
    }
    
    private void philosophePrendChopstick () {
        verifieChopsticksLibre(droite, gauche);
        prendLesChopstick(droite, gauche);
    }
    
    private void verifieChopsticksLibre (int droite, int gauche) {
        tableManger.verifieLibre(droite, gauche);
        
    }

    private void prendLesChopstick (int premierCoteChopstick, int deuxiemeCoteChopstick) {
        tableManger.prendre(premierCoteChopstick, deuxiemeCoteChopstick);
        tableManger.prendreChopstick(idDuPhilosophe, premierCoteChopstick, deuxiemeCoteChopstick);
    }
    
    private void philosopheMangeAvecChopstick () {
        repos((long) (Math.random() * TEMPS_MAXIMAL_MANGER)); 
    }
    
    private void philosophelibereChopstick () {
        liberelesChopstick(droite);
        liberelesChopstick(gauche);
    }
        
    private void liberelesChopstick (int coteDuChopstick) {
        tableManger.libereChopstick(coteDuChopstick);
        tableManger.libere(coteDuChopstick);
    }
    
    private void repos (long tempsDeRepos) {
        try {
            sleep(tempsDeRepos);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

}
