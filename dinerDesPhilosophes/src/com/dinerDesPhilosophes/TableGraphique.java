package com.dinerDesPhilosophes;

import static autre.Constante.*;

import java.awt.*;
import java.awt.event.*;

@SuppressWarnings("serial")
public class TableGraphique extends Frame implements WindowListener {
    static private int compte = 0;
    static private int enAttente = 0;
    private Point centreDeAssiete;
    private AssieteGraphique assiete[];
    private ChopstickGraphique chops[];
    private boolean chopsticks[];

    @SuppressWarnings("deprecation")
    public TableGraphique() {
        super();
        initialisationInterfaceGraphic();
        show();
        setResizable(ECRAN_AJUSTABLE);
        initialisationPhilosophe();
    }

    public synchronized void devientAffame (int idPhilosophe) {
        while (compte == NOMBRE_DE_PHILOSOPHES || enAttente > AUCUN_PHILOSOPHES) {
            attente();
        }
        compte++;
        assiete[idPhilosophe].definiCouleurPhilosophe(idPhilosophe);
        repaint();
    }

    public synchronized void enTrainDeReflechir (int idPhilosophe) {
        assiete[idPhilosophe].definiCouleurPhilosophe(ID_PAR_DEFAUT_PHILOSOPHE);
        repaint();
        compte--;
        notify();
    }

    public synchronized void verifieLibre (int premierCoteChopstick, int deuxiemeCoteChopstick) {
        while (!chopsticks[premierCoteChopstick] || !chopsticks[deuxiemeCoteChopstick]) {
            try {
                enAttentePourPrendreChopstick();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public synchronized void prendre (int premierCoteChopstick, int deuxiemeCoteChopstick) {
        chopsticks[premierCoteChopstick] = CHOPSTICK_PRIT;
        chopsticks[deuxiemeCoteChopstick] = CHOPSTICK_PRIT;
    }
    
    public synchronized void libere (int coteDuChopstick) {
        chopsticks[coteDuChopstick] = CHOPSTICK_LIBRE;
        notifyAll();

    }

    public void prendreChopstick (int idPhilosophe, int premierCoteChopstick,
            int deuxiemeCoteChopstick) {
        chops[premierCoteChopstick].definiCouleurPhilosophe(idPhilosophe);
        chops[deuxiemeCoteChopstick].definiCouleurPhilosophe(idPhilosophe);
        repaint();
    }

    public void libereChopstick (int chID) {
        chops[chID].definiCouleurPhilosophe(ID_PAR_DEFAUT_PHILOSOPHE);
        repaint();
    }

    public void paint (Graphics g) {
        for (int i = AUCUN_PHILOSOPHES; i < NOMBRE_DE_PHILOSOPHES; i++) {
            assiete[i].dessine(g);
            chops[i].dessine(g);
        }
    }
    
    public void windowClosing (WindowEvent evt) {
        System.exit(0);
    }
    
    private void initialisationPhilosophe () {
        for (int i = AUCUN_PHILOSOPHES; i < NOMBRE_DE_PHILOSOPHES; i++) {
            creationPhilosophe(i);
        }
    }
    
    private void creationPhilosophe (int i) {
        int idPhilosophe = i;
        int positionChopstickDroit = i;
        int positionChopstickGauche = (i + NOMBRE_DE_PHILOSOPHE_MOINS_LUI_MEME) 
                % NOMBRE_DE_PHILOSOPHES;
        new Philosophe(idPhilosophe, this, 
                positionChopstickDroit, positionChopstickGauche).start();
    }
    
    private void initialisationInterfaceGraphic () {
        initialisationEnteteInterfaceGraphic();
        initialisationPointCentral();
        initialisationAssiete();
        initialisationChopstick();   
    }
    
    private void initialisationEnteteInterfaceGraphic () {
        addWindowListener(this);
        setTitle(TEXTE_TITRE_PROJET);
        setSize(TAILLE_INTERFACE_TABLE_PHILOSOPHE, TAILLE_INTERFACE_TABLE_PHILOSOPHE);
        setBackground(COULEUR_INTERFACE);
    }
    
    private void initialisationPointCentral () {
        centreDeAssiete = new Point(TAILLE_INTERFACE_TABLE_PHILOSOPHE / 2, 
                TAILLE_INTERFACE_TABLE_PHILOSOPHE / 2);
    }
    
    private void initialisationAssiete () {
        assiete = new AssieteGraphique[NOMBRE_DE_PHILOSOPHES];
        for (int i = AUCUN_PHILOSOPHES; i < NOMBRE_DE_PHILOSOPHES; i++) {
            assiete[i] = new AssieteGraphique(i, centreDeAssiete, 
                    new Point(centreDeAssiete.x, centreDeAssiete.y - COORD_SOIXANTEDIX),
                        POINT_VINGT);
        }
    }
    
    private void initialisationChopstick () {
        creationNouveauTableauPourChopstick();
        for (int i = AUCUN_PHILOSOPHES; i < NOMBRE_DE_PHILOSOPHES; i++) {
            initialisationValeursPourChopstick(i);
        }
    }
        
    private void creationNouveauTableauPourChopstick (){
        chopsticks = new boolean[NOMBRE_DE_PHILOSOPHES];
        chops = new ChopstickGraphique[NOMBRE_DE_PHILOSOPHES];
    }
        
    private void initialisationValeursPourChopstick (int positionRenduDansTableau) {
        chopsticks[positionRenduDansTableau] = CHOPSTICK_LIBRE;            
        chops[positionRenduDansTableau] = new ChopstickGraphique(positionRenduDansTableau, 
                centreDeAssiete, 
                new Point(centreDeAssiete.x, centreDeAssiete.y - COORD_SOIXANTEDIX), 
                new Point(centreDeAssiete.x, centreDeAssiete.y - COORD_QUARANTE));
    }
    
    private void enAttentePourPrendreChopstick () throws InterruptedException{
        wait();
    }

    private synchronized void attente () {
        try {
            enAttente++;
            wait();
            enAttente--;
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
    
    public void windowOpened (WindowEvent evt) {}
    public void windowClosed (WindowEvent evt) {}
    public void windowIconified (WindowEvent evt) {}
    public void windowDeiconified (WindowEvent evt) {}
    public void windowActivated (WindowEvent evt) {}
    public void windowDeactivated (WindowEvent evt) {}
}
