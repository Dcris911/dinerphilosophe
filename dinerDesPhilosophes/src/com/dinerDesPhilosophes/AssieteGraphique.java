package com.dinerDesPhilosophes;

import static autre.Constante.*;

import java.awt.*;

public class AssieteGraphique extends DinerGraphique {
  private Point coords;
  private int grandeur;
  private int identifiant;

    AssieteGraphique (int ident, Point center, Point coords, int size) {
        super();
        initialisationVariable(ident, center, coords, size);
    }

    public void dessine (Graphics graphique) {
        graphique.setColor(couleur);
        graphique.fillOval(coords.x, coords.y, grandeur, grandeur);
    }
  
    private void initialisationVariable (int ident, Point centre, Point coords, int grandeur) {
        initialisationVariableMembre(ident, grandeur);
        initialisationCoordonne(centre, coords);
    }
    
    private void initialisationVariableMembre (int ident, int grandeur) {
        this.identifiant = ident;
        this.grandeur = grandeur;
    }

    private void initialisationCoordonne (Point centre, Point coords) {
        this.angle = UN_CINQUIEME_CERCLE * this.identifiant;
        this.coords = new Point(deplacement(coords.x, coords.y, centre.x, centre.y));
        this.coords.x -= COORD_DIX;
        this.coords.y += COORD_CINQ;   
    }

    //TODO Dieu, svp, refactor moi pour la prochaine fois que jouvre eclipse!
    private Point deplacement (int x, int y, int bX, int bY) {
        double rotation = Math.PI / (DEMI_CERCLE / this.angle);
        Point p = new Point();
        p.y = (int) (x * Math.sin(rotation) + y * Math.cos(rotation)
                - Math.sin(rotation) * bX - Math.cos(rotation) * bY + bY);
        p.x = (int) (x * Math.cos(rotation) - y * Math.sin(rotation) 
                - Math.cos(rotation) * bX + Math.sin(rotation) * bY + bX);
        return(p);
    }
  
}
  
