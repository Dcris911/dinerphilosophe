package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import static autre.Constante.*;
import com.dinerDesPhilosophes.DinerGraphique;;

//import static org.assertj.core.api.Assertions.*;

/**
 * @author David Gilbert
 */
public class DDPTestDinerGraphique {
    
    private DinerGraphique dinerGraph;
    
    @Before
    public void setUp () throws Exception {
        dinerGraph = new DinerGraphique();
    }
    
    @Test
    public void testCouleurBase () throws Exception {
        assertEquals(COULEUR_AUCUN_PHILOSOPHE, 
            dinerGraph.definiCouleurPhilosophe(ID_PAR_DEFAUT_PHILOSOPHE));
    }
    
    @Test
    public void testCouleurPhilosophe1 () throws Exception {
        assertEquals(COULEUR_PHILOSOPHE_1, dinerGraph.definiCouleurPhilosophe(ID_PHILOSOPHE_1));
    }
    
    @Test
    public void testCouleurPhilosophe5 () throws Exception {
        assertEquals(COULEUR_PHILOSOPHE_5, dinerGraph.definiCouleurPhilosophe(ID_PHILOSOPHE_5));
    }
    
    @Test
    public void testCouleurPhilosophe3 () throws Exception {
        assertEquals(COULEUR_PHILOSOPHE_3, dinerGraph.definiCouleurPhilosophe(ID_PHILOSOPHE_3));
    }
}
